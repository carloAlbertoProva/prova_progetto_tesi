#!/bin/bash

set -e

gcloud --version

#Installo l'ultima versione di Google Cloud SDK
if [ ! -d "$HOME/google-cloud-sdk/bin" ]; then rm -rf $HOME/google-cloud-sdk; export CLOUDSDK_CORE_DISABLE_PROMPTS=1; curl https://sdk.cloud.google.com | bash; fi

source /home/travis/google-cloud-sdk/path.bash.inc

gcloud --quiet version
gcloud --quiet components update
gcloud --quiet components update kubectl
gcloud components update

gcloud --version

#GCLOUD_SERVICE_KEY_PROD è praticamente la chiave json generata per l'account di servizio, criptata precedentemente sul mio terminale con 
#"base64 --wrap=0 [your-credentials.json]"
#e salvata in Travis --> settings --> Environment Variables
echo $GCLOUD_SERVICE_KEY_PROD | base64 --decode -i > ${HOME}/client-secret.json

gcloud auth activate-service-account ${ACCOUNT_SERVIZIO} --key-file ${HOME}/client-secret.json

#error=$(comando 2>&1)
#echo $error

#verifico se ci sono delle immagini docker vecchie - verifico se esistono tag attivi
tags=$(gcloud container images list-tags gcr.io/booming-entity-280015/demo-app-university  | sed -n 2p | awk '{ print $2; }')
#se ci sono tag cancello
[ -z $tags ] && echo "no docker images" || yes | gcloud container images delete gcr.io/${PROJECT_PROD}/${APP_IMAGE}:latest --force-delete-tags 2>&1>/dev/null

gcloud --quiet config set project $PROJECT_PROD
gcloud --quiet config set compute/zone ${ZONE}

#creo immagine da pushare su google cloud

docker build -t gcr.io/${PROJECT_PROD}/${APP_IMAGE}:$TRAVIS_COMMIT -f docker/Dockerfile .

#pusho sul container registry di google cloud l'immagine

gcloud docker -- push gcr.io/${PROJECT_PROD}/${APP_IMAGE}

#taggo l'immagine deployata sia con il tag del commit, che come "latest"

yes | gcloud beta container images add-tag gcr.io/${PROJECT_PROD}/${APP_IMAGE}:$TRAVIS_COMMIT gcr.io/${PROJECT_PROD}/${APP_IMAGE}:latest

#deploy di un container a partire dall'immagine pushata sul servizio "cloud run fully managed" (--platform managed) nella regione us-central1-c con una memoria allocata di 1 Giga 
#con il nome "uni-devops-project-app"
#e lo espongo sul web(--allow-unauthenticated)

gcloud run deploy uni-devops-project-app --image gcr.io/${PROJECT_PROD}/${APP_IMAGE}:latest --allow-unauthenticated --platform managed --region ${ZONE} --memory=1Gi

